#include "hal.h"


unsigned c_hal_read32(unsigned a) {
	//__asm__("lw\ta0, (a0)"::);
	return *(volatile unsigned *)a;
}


void c_hal_write32(unsigned a, unsigned d) {
	//__asm__("sw\ta1, (a0)"::);
	*(volatile unsigned *)a = d;
}


void c_hal_wait_for_irq() {}


void c_cpu_relax() {}


void c_printf(const char *str) {
	while (*str) {
		hal_write32(0x4, *(unsigned *)str);
		str += 4;
	}
}
