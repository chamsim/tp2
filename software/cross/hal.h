/*\
 * vim: tw=0: cindent: sw=3: ts=3: sts=3: noet: nolist:
 * Copyright (C) 2009, 2012 by Verimag
 * Initial author: Matthieu Moy
 * riscv support : Frédéric Pétrot <frederic.petrot@univ-grenoble-alpes.fr
\*/

/*\
 * Harwdare Abstraction Layer : implementation for risc-v
\*/
#ifndef HAL_H
#define HAL_H

#include <stdint.h>


/* Dummy implementation of abort(): invalid instruction */
#define abort() do {				\
	printf("abort() function called\r\n");  \
	_hw_exception_handler();		\
} while (0)

unsigned c_hal_read32(unsigned a);

void c_hal_write32(unsigned a, unsigned d);

void c_hal_wait_for_irq();

void c_cpu_relax();

void c_printf(const char *str);

/* TODO: implement HAL primitives for cross-compilation */
#define hal_read32(a)      c_hal_read32(a)
#define hal_write32(a, d)  c_hal_write32(a, d)
#define hal_wait_for_irq() c_hal_wait_for_irq()
#define hal_cpu_relax()    c_cpu_relax()

static inline void enable_interrupts(void) {
	__asm("li    t0, 0x8\n"
	      "csrs  mstatus, t0\n"
			"li    t0, 0x800\n"
			"csrs  mie, t0");
}

/* TODO: printf is disabled, for now ... */
#define printf(...) c_printf(__VA_ARGS__)

#endif /* HAL_H */
